<?php

namespace Drupal\uptime_robot;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp\ClientInterface;
use Drupal\Component\Serialization\Json;
use GuzzleHttp\Exception\GuzzleException;
use Drupal\Component\Serialization\Exception\InvalidDataTypeException;

/**
 * Class to interface with the Uptime Robot REST API.
 *
 * @link https://uptimerobot.com/api/
 */
class UptimeRobotAPI implements ContainerInjectionInterface {

  /**
   * The api endpoint.
   *
   * @var string
   */
  protected $apiEndpoint = 'https://api.uptimerobot.com/v2';

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * The http client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The JSON serializer.
   *
   * @var \Drupal\Component\Serialization\Json
   */
  protected $jsonSerializer;

  /**
   * Constructs a new UptimeRobotAPI object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The http client.
   * @param \Drupal\Component\Serialization\Json $json_serializer
   *   The JSON serializer.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ClientInterface $http_client, Json $json_serializer) {
    $this->config = $config_factory->get('uptime_robot.settings');
    $this->httpClient = $http_client;
    $this->jsonSerializer = $json_serializer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('http_client'),
      $container->get('serialization.json')
    );
  }

  /**
   * Return Account details.
   *
   * @return array
   *   The account details.
   */
  public function getAccountDetails() {
    $data = $this->makeRequest('getAccountDetails');
    return $data['account'];
  }

  /**
   * Make a request to the Uptime Robot API.
   *
   * @param string $endpoint
   *   The endpoint.
   *
   * @return mixed
   *   The decoded JSON response.
   */
  private function makeRequest($endpoint, $data = []) {
    $api_key = $this->config->get('api_key');

    $form_params = [
      'api_key' => $api_key,
      'format' => 'json',
    ] + $data;
    try {
      /** @var \GuzzleHttp\Psr7\Response $response */
      $response = $this->httpClient->request('POST', "https://api.uptimerobot.com/v2/$endpoint", [
        'headers' => [
          'content-type' => 'application/x-www-form-urlencoded',
          'cache-control' => 'no-cache',
        ],
        'form_params' => $form_params,
      ]);
    }
    catch (GuzzleException $e) {
      \Drupal::logger('uptime_robot')->error($e->getMessage());
    }

    $body = (string) $response->getBody();
    try {
      $data = $this->jsonSerializer->decode($body);
    }
    catch (InvalidDataTypeException $e) {
      \Drupal::logger('uptime_robot')->error($e->getMessage());
    }

    return $data;
  }

  /**
   * Retrieves all monitors.
   */
  public function getMonitors() {
    // Maybe a function to import monitors.
  }

  /**
   * Creates a monitor.
   */
  public function createMonitor($friendly_name, $url, $type = 1, $options = []) {

  }

  /**
   * Deletes a monitor.
   *
   * @param int $id
   *   The monitor id.
   */
  public function deleteMonitor($id) {

  }

  /**
   * Creates an alert contact.
   *
   * @param int $id
   *   The alert contact id.
   */
  public function createAlertContact($id) {

  }

  /**
   * Deletes an alert contact.
   */
  public function deleteAlertContact() {

  }

}
