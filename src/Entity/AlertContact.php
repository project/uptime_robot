<?php

namespace Drupal\uptime_robot\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines an Alert Contact configuration entity class.
 *
 * @ConfigEntityType(
 *   id = "uptime_robot_alert_contact",
 *   label = @Translation("Alert Contact"),
 *   label_collection = @Translation("Alert Contacts"),
 *   label_singular = @Translation("alert contact"),
 *   label_plural = @Translation("alert contacts"),
 *   label_count = @PluralTranslation(
 *     singular = "@count alert contact",
 *     plural = "@count alert contacts",
 *   ),
 *   handlers = {
 *     "form" = {
 *       "default" = "Drupal\uptime_robot\UptimeRobotAlertContactForm",
 *       "edit" = "Drupal\uptime_robot\UptimeRobotAlertContactForm",
 *     },
 *     "list_builder" = "Drupal\uptime_robot\UptimeRobotAlertContactListBuilder"
 *   },
 *   admin_permission = "administer uptime_robot alert contacts",
 *   entity_keys = {
 *     "id" = "id"
 *   },
 *   links = {
 *     "delete-form" = "/admin/config/services/uptime_robot/manage/{uptime_robot_alert_contact}/delete",
 *     "edit-form" = "/admin/config/services/uptime_robot/manage/{uptime_robot_alert_contact}",
 *   },
 *   config_export = {
 *     "id",
 *     "type",
 *     "value",
 *     "friendly_name",
 *   },
 *   lookup_keys = {
 *     "friendly_name"
 *   }
 * )
 */
class AlertContact extends ConfigEntityBase {

  /**
   * The ID of the block.
   *
   * @var string
   */
  protected $id;

  /**
   * The type.
   *
   * @var string
   */
  protected $type;

  /**
   * The value.
   *
   * @var string
   */
  protected $value;

  /**
   * The friendly name.
   *
   * @var string
   */
  protected $friendlyName;

  /**
   * Get the type.
   *
   * @return string
   *   The type.
   */
  public function getType() {
    return '';
  }

  /**
   * Set the type.
   *
   * @param string $type
   *   The type.
   */
  public function setType($type) {

  }

  /**
   * Get the value.
   *
   * @return string
   *   The value.
   */
  public function getValue() {
    return '';
  }

  /**
   * Set the value.
   *
   * @param string $value
   *   The value.
   */
  public function setValue($value) {

  }

  /**
   * Get the friendly name.
   *
   * @return string
   *   The friendly name.
   */
  public function getFriendlyName() {
    return '';
  }

  /**
   * Set the friendly name.
   *
   * @param string $friendly_name
   *   The friendly name.
   */
  public function setFriendlyName($friendly_name) {

  }

}
