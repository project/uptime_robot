<?php

namespace Drupal\uptime_robot\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Uptime Robot settings for this site.
 *
 * @internal
 */
class UptimeRobotAccountSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'uptime_robot_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['uptime_robot.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('uptime_robot.settings');

    /** @var \Drupal\Core\Cache\CacheBackendInterface $cache */
    $cache = \Drupal::service('cache.default');

    /** @var \Drupal\uptime_robot\UptimeRobotAPI $api */
    $api = \Drupal::service('uptime_robot.api');

    $monitor_limit = NULL;
    if ($cached_data = $cache->get('uptime_robot.monitor_limit')) {
      $monitor_limit = $cached_data->data;
    }
    else {
      $account = $api->getAccountDetails();

      $monitor_limit = $account['monitor_limit'];
      $total_monitors_count = $account['total_monitors_count'];
      $cache->set('uptime_robot.monitor_limit', $monitor_limit, time() + 3600);
      $cache->set('uptime_robot.total_monitors_count', $total_monitors_count, time() + 3600);
    }

    $form['monitor_limit'] = [
      '#markup' => $this->t('Monitor Limit: :monitor_limit', [':monitor_limit' => $monitor_limit]),
    ];

    $form['total_monitors_count'] = [
      '#markup' => $this->t('Total Monitors: :total_monitors_count', [':total_monitors_count' => $total_monitors_count]),
    ];

    $form['monitors_left'] = [
      '#markup' => $this->t('Monitors Left: :monitors_left', [':monitors_left' => $monitor_limit - $total_monitors_count]),
    ];

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#default_value' => $config->get('api_key'),
      '#description' => $this->t('You must generate a Main API Key.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('uptime_robot.settings')
      ->set('api_key', $form_state->getValue('api_key'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
