<?php

namespace Drupal\uptime_robot\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Uptime Robot settings for this site.
 *
 * @internal
 */
class UptimeRobotAlertContactForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'uptime_robot_alert_contacts';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['uptime_robot.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('uptime_robot.settings');

    // type, value, friendly name
    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#default_value' => $config->get('api_key'),
      '#description' => $this->t('You must generate a Main API Key.'),
    ];

    $form['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Type'),
      '#options' => [
        'something',
        'another',
      ],
      '#default_value' => $config->get('type'),
    ];

    $form['value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Value'),
      '#default_value' => $config->get('value'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $contact = [
      'friendly_name' => $form_state->getValue('name'),
      'type' => $form_state->getValue('type'),
      'value' => $form_state->getValue('value'),
    ];
    $contacts = $this->config('uptime_robot.settings')->get('alert_contacts', []);
    $contacts[] = $contact;
    $this->config('uptime_robot.settings')
      ->set('alert_contacts', $contacts)
      ->save();

    parent::submitForm($form, $form_state);
  }

}
