<?php

namespace Drupal\uptime_robot\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'comment_username' formatter.
 *
 * @FieldFormatter(
 *   id = "uptime_robot_monitor",
 *   label = @Translation("Uptime Robot Monitor"),
 *   description = @Translation("Display the Uptime Robot Monitor."),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class MonitorFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      /** @var \Drupal\comment\CommentInterface $comment */
      $comment = $item->getEntity();
      $account = $comment->getOwner();
      $elements[$delta] = [
        '#theme' => 'username',
        '#account' => $account,
        '#cache' => [
          'tags' => $account->getCacheTags() + $comment->getCacheTags(),
        ],
      ];
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return $field_definition->getName() === 'name' && $field_definition->getTargetEntityTypeId() === 'comment';
  }

}
