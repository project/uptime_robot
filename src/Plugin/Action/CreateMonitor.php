<?php

namespace Drupal\uptime_robot\Plugin\Action;

use Drupal\Core\Action\ActionBase;
use Drupal\Core\Session\AccountInterface;

/**
 * Creates a monitor.
 *
 * @Action(
 *   id = "uptime_robot_create_monitor_action",
 *   label = @Translation("Demote selected content from front page"),
 *   type = "node"
 * )
 */
class CreateMonitor extends ActionBase {

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    // Create monitor entity.


    // Create monitor in Uptime Robot.
    \Drupal::service('uptime_robot.api')->createMonitor();
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    /** @var \Drupal\Core\Access\AccessResultInterface $result */
    $result = $object->access('update', $account, TRUE);

    foreach ($this->getFieldsToUpdate() as $field => $value) {
      $result->andIf($object->{$field}->access('edit', $account, TRUE));
    }

    return $return_as_object ? $result : $result->isAllowed();
  }

}
