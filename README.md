# Uptime Robot

This module integrates with the Uptime Robot service to manage monitors and
their alert contacts.


## TODO

- [ ] Export default view for monitors.
- [ ] Config Form
- [x] If API Key is not defined, throw error on status page
- [ ] If API Key doesn't successfully authenticate, throw error on status page (run this on a cron. default 1 hour, configurable)
- [ ] Monitor entity should be fieldable (maybe tags field should be added by default)
- [ ] Creating a monitor should be an action
- [ ] Under 'Reports' there should be a link 'Uptime Robot Monitors' (view)
- [ ] Uptime Robot config should have a parent menu item that contains api configuration and alert contact configuration
